#include "AirBreak.h"

extern "C" bool addHelper( HelperInfo &helper );
extern "C" bool statusHelper( std::string_view helper );
extern "C" bool toggleHelper( std::string_view helper, const bool &status );

AirBreak::AirBreak( SRDescent *parent )
	: SRDescent( parent ), helper( &status, { VK_LCONTROL, VK_LSHIFT, 'A' }, "AirBreak" ) {
	helper.statusChanged = []( bool status ) {
		CPhysical *phys = LOCAL_PLAYER;
		if ( LOCAL_PLAYER->isDriving() ) phys = LOCAL_PLAYER->vehicle;
		if ( !status ) {
			phys->unlock();
			phys->soft			 = false;
			phys->bUsesCollision = true;
			CPad::get()->bDisablePlayerJump = false;
		}
	};
	helper.mainloop = [this] {
		if ( !status ) return;
		if ( !LOCAL_PLAYER ) return;
		if ( !LOCAL_PLAYER->isValid() ) return;
		if ( LOCAL_PLAYER->isActorDead() ) return;
		CPhysical *phys = LOCAL_PLAYER;
		if ( LOCAL_PLAYER->isDriving() ) phys = LOCAL_PLAYER->vehicle;

		if ( !phys->isLocked() ) {
			phys->lock();
			phys->soft			 = true;
			phys->bUsesCollision = false;
			CPad::get()->bDisablePlayerJump = true;
		}
		RwMatrix camMat = CCamera::TheCamera()->m_cameraMatrix;
		camMat.right *= -1;
		float camAngle		   = *(float *)0xB6F258 + float( 3.14159 ) / 2.0f;
		phys->matrix->right.fX = cosf( -camAngle );
		phys->matrix->right.fY = -sinf( -camAngle );
		phys->matrix->right.fZ = 0.0f;
		phys->matrix->up.fX	   = sinf( -camAngle );
		phys->matrix->up.fY	   = cosf( -camAngle );
		phys->matrix->up.fZ	   = 0.0f;
		phys->matrix->at	   = { 0.0f, 0.0f, 1.0f };

		auto moveTo = [&]( RwV3D vec ) {
			for ( int i = 0; i < 3; ++i )
				if ( vec.f[i] ) move.f[i] = vec.f[i];
			moving = true;
		};
		auto keysNotPressed = 0;
		if ( !LOCAL_PLAYER->isDriving() ) {
			LOCAL_PLAYER->fCurrentRotation		= camAngle;
			LOCAL_PLAYER->fTargetRotation		= camAngle;
			LOCAL_PLAYER->pedFlags.bIsStanding	= true;
			LOCAL_PLAYER->pedFlags.bWasStanding = true;
			if ( speed > 0.55f ) speed = 0.55f;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Forward_or_Backward ) == 65408 )
				moveTo( { 0.0f, 1, 0.0f } );
			else
				++keysNotPressed;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Forward_or_Backward ) == 128 )
				moveTo( { 0.0f, -1, 0.0f } );
			else
				++keysNotPressed;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Jump ) )
				moveTo( { 0.0f, 0.0f, 1 } );
			else
				++keysNotPressed;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Sprint ) )
				moveTo( { 0.0f, 0.0f, -1 } );
			else
				++keysNotPressed;
		} else {
			memsafe::write( 0xB70118, 50.0f );
			phys->matrix->right = camMat.right;
			phys->matrix->up	= camMat.up;
			phys->matrix->at	= camMat.at;
			if ( speed > LOCAL_PLAYER->vehicle->pHandlingData->transmissionData.fMaxVelocity )
				speed = LOCAL_PLAYER->vehicle->pHandlingData->transmissionData.fMaxVelocity;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Accelerate ) )
				moveTo( { 0.0f, 1, 0.0f } );
			else
				++keysNotPressed;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Break_or_Reserve ) )
				moveTo( { 0.0f, -1, 0.0f } );
			else
				++keysNotPressed;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Steer_Back_or_Up ) == 128 )
				moveTo( { 0.0f, 0.0f, 1 } );
			else
				++keysNotPressed;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Steer_Back_or_Up ) == 65408 )
				moveTo( { 0.0f, 0.0f, -1 } );
			else
				++keysNotPressed;
		}
		if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Left_or_Right ) == 65408 )
			moveTo( { -1, 0.0f, 0.0f } );
		else
			++keysNotPressed;
		if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Left_or_Right ) == 128 )
			moveTo( { 1, 0.0f, 0.0f } );
		else
			++keysNotPressed;
            
		if ( keysNotPressed == 6 ) {
			moving = false;
			accel  = 0.0f;
			phys->speed.clear();
			move.clear();
		} else {
			if ( accel < speed )
				accel += 0.0035f * ( 60.0f / *(float *)0xB7CB50 );
			else
				accel = speed;
			move.normalize();
			for ( int i = 0; i < 3; ++i )
				if ( move.f[i] != 0 ) move.f[i] *= accel;
			phys->speed = camMat.getOffset( move );
			move *= 60.0f / *(float *)0xB7CB50;
			if ( !LOCAL_PLAYER->isDriving() ) phys->matrix->pos += camMat.getOffset( move );
			move.clear();
		}
	};
	helper.render = [this]() -> std::string { return "AirBreak: " + std::to_string( speed ); };
	addHelper( helper );

	ApplyMoveSpeed.install();
	ApplyMoveSpeed.onBefore += [this]( SRHook::Info &info ) {
		info.skipOriginal = true;
		if ( !status || !moving ||
			 ( info.cpu.ECX != size_t( LOCAL_PLAYER ) && info.cpu.ECX != size_t( LOCAL_PLAYER->vehicle ) ) ) {
			info.cpu.EAX = *(size_t *)( info.cpu.ECX + 0x40 );
			if ( info.cpu.AH & 0x20 || info.cpu.AL & 0x20 ) info.retAddr = 0x542E13;
		}
	};
	ProcessControl.install();
	ProcessControl.onAfter += [this]( SRHook::CPU &cpu ) {
		if ( status && ( cpu.ECX == size_t( LOCAL_PLAYER ) || cpu.ECX == size_t( LOCAL_PLAYER->vehicle ) ) ) cpu.ZF = 1;
	};
	UpdatePosition.install();
	UpdatePosition.onAfter += [this]( SRHook::CPU &cpu ) {
		if ( status && cpu.ECX == size_t( LOCAL_PLAYER )  ) cpu.ZF = 1;
	};

	g_class.events->onScrollUp += [this] {
		if ( !g_class.events->isKeyDown( VK_LMENU ) || !status ) return;
		speed += 0.025f;
	};
	g_class.events->onScrollDown += [this] {
		if ( !g_class.events->isKeyDown( VK_LMENU ) || !status ) return;
		if ( speed > 0.05 ) speed -= 0.025f;
	};
	addHelper( helper );
}

AirBreak::~AirBreak() {}
