#ifndef LOADER_H
#define LOADER_H

#include <base.h>

struct HelperInfo {
	bool &						 status;
	std::deque<int>				 combo;
	std::string					 name;
	std::function<void()>		 mainloop	   = [] {};
	std::function<void( bool )>	 statusChanged = []( bool ) {};
	std::function<std::string()> render		   = [this] { return name; };

	HelperInfo( bool *status, std::deque<int> combo, std::string_view name )
		: status( *status ), combo( combo ), name( name ) {}
};

/**
 * \brief Выводит окно с сообщениемю
 * \detail Является оберткой над MessageBoxA, для более удобного вывода сообщений.
 * \param[in] text Текст сообщения.
 * \param[in] title Заголовок окна с сообщением.
 * \param[in] type Тип окна.
 * \return код завершения окна (нажатая кнопка).
 */
int MessageBox( std::string_view text, std::string_view title = PROJECT_NAME, UINT type = MB_OK );

#endif // LOADER_H
